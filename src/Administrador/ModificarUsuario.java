package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

public class ModificarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID1;
	private JTextField txtNom1;
	private JTextField txtNip1;
	private JTextField txtEfectivo;
	private JLabel lblNewLabel_4;
	private JButton btnNewButton;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarUsuario frame = new ModificarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarUsuario() {
		setTitle("BanMex");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setForeground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID: ");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(51, 73, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(51, 104, 79, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("NIP:");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setBounds(51, 129, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		txtID1 = new JTextField();
		txtID1.setBounds(164, 71, 86, 17);
		contentPane.add(txtID1);
		txtID1.setColumns(10);
		
		txtNom1 = new JTextField();
		txtNom1.setBounds(164, 99, 86, 17);
		contentPane.add(txtNom1);
		txtNom1.setColumns(10);
		
		txtNip1 = new JTextField();
		txtNip1.setBounds(164, 127, 86, 17);
		contentPane.add(txtNip1);
		txtNip1.setColumns(10);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				MetodosAdmin metodos=new  MetodosAdmin();
				User usuario=new User();
				
				usuario.setIdUsuario(Integer.parseInt(txtID1.getText()));
				usuario= metodos.seleccionarUsuario(usuario);
				
				int id= (usuario.getIdUsuario());
				String Nombre=(txtNom1.getText());
				int numTarjeta=(usuario.getNumTarjeta());
				int NIP=(Integer.parseInt(txtNip1.getText()));
				
				Double fondoInicial=Double.parseDouble(txtEfectivo.getText());
				Double FondoInicial1=(usuario.getFondoInicial());
				Double fondoFinal=fondoInicial;
				
				metodos.actualizarRegistro(id, Nombre, fondoFinal, numTarjeta, NIP);
				JOptionPane.showMessageDialog(null, "Los datos Actualizados son: \n "+ usuario, getTitle(), JOptionPane.WARNING_MESSAGE);
	
				
				
			}
		});
		btnActualizar.setBounds(164, 183, 86, 17);
		contentPane.add(btnActualizar);
		
		txtEfectivo = new JTextField();
		txtEfectivo.setBounds(164, 155, 86, 17);
		contentPane.add(txtEfectivo);
		txtEfectivo.setColumns(10);
		
		lblNewLabel_4 = new JLabel("Saldo:");
		lblNewLabel_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_4.setBounds(51, 157, 67, 14);
		contentPane.add(lblNewLabel_4);
		
		btnNewButton = new JButton("Regresar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				MenuPrincipal principal = new MenuPrincipal();
				principal.setVisible(true);
				
				
			}
		});
		btnNewButton.setBounds(10, 227, 94, 23);
		contentPane.add(btnNewButton);
		
		lblNewLabel_2 = new JLabel("Modificador De Usuarios.");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(91, 11, 241, 28);
		contentPane.add(lblNewLabel_2);
	}
}
