package Administrador;

import java.util.Random;

public class User {
	
 	private int idUsuario;
    private String nombreUsuario;
    private int nip;
    private int numTarjeta=55790830;
    private Double fondoInicial;
	
	


    public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public int getNip() {
		return nip;
	}
	public void setNip(int nip) {
		this.nip = nip;
	}
	public int getNumTarjeta() {
		return numTarjeta;
	}
	public void setNumTarjeta(int numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	public Double getFondoInicial() {
		return fondoInicial;
	}
	public void setFondoInicial(Double fondoInicial) {
		this.fondoInicial = fondoInicial;
	}
	
	 @Override
	    public String toString() {
	        return "Usuario"
	        		+ "\n" + "ID: " + idUsuario + "\n Nombre: " + nombreUsuario+ "\n"
	        			+ "No. Tarjeta: " + numTarjeta +"\n NIP: " + nip + "\n fondo: $" + fondoInicial ;
	    }
	 }
